%global qt_module qtx11extras

Summary: 	   Qt5 - X11 support library
Name:    	   qt5-%{qt_module}
Version: 	   5.15.10
Release: 	   1

# See LGPL_EXCEPTIONS.txt, LICENSE.GPL3, respectively, for exception details
License: 	   LGPL-3.0-only OR GPL-3.0-only WITH Qt-GPL-exception-1.0
Url:     	   http://www.qt.io
%global majmin %(echo %{version} | cut -d. -f1-2)
Source0: 	   https://download.qt.io/official_releases/qt/%{majmin}/%{version}/submodules/%{qt_module}-everywhere-opensource-src-%{version}.tar.xz

BuildRequires: make
BuildRequires: qt5-qtbase-devel >= %{version}
BuildRequires: qt5-qtbase-private-devel
%{?_qt5:Requires: %{_qt5}%{?_isa} = %{_qt5_version}}

%description
The X11 Extras module provides features specific to platforms using X11, e.g.
Linux and UNIX-like systems including embedded Linux systems that use the X
Window System.

%package devel
Summary: 	   Development files for %{name}
Requires: 	   %{name}%{?_isa} = %{version}-%{release}
Requires:      qt5-qtbase-devel%{?_isa}
%description devel
%{summary}.


%prep
%autosetup -n %{qt_module}-everywhere-src-%{version}


%build
%{qmake_qt5}

%make_build


%install
make install INSTALL_ROOT=%{buildroot}
%delete_la
pushd %{buildroot}%{_qt5_libdir}
for prl_file in libQt5*.prl ; do
    sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" ${prl_file}
    if [ -f "$(basename ${prl_file} .prl).so" ]; then
        rm -fv "$(basename ${prl_file} .prl).la"
	sed -i -e "/^QMAKE_PRL_LIBS/d" ${prl_file}
    fi
done
popd

%ldconfig_scriptlets

%files
%license LICENSE.GPL3-EXCEPT LICENSE.LGPL* LICENSE.GPL*
%{_qt5_libdir}/libQt5X11Extras.so.5*

%files devel
%{_qt5_headerdir}/QtX11Extras/
%{_qt5_libdir}/libQt5X11Extras.so
%{_qt5_libdir}/libQt5X11Extras.prl
%dir %{_qt5_libdir}/cmake/Qt5X11Extras/
%{_qt5_libdir}/cmake/Qt5X11Extras/Qt5X11ExtrasConfig*.cmake
%{_qt5_libdir}/pkgconfig/Qt5X11Extras.pc
%{_qt5_archdatadir}/mkspecs/modules/qt_lib_x11extras*.pri


%changelog
* Wed Aug 23 2023 douyan <douyan@kylinos.cn> - 5.15.10-1
- update to upstream version 5.15.10

* Wed Jul 13 2022 Chenyx <chenyixiong3@huawei.com> - 5.15.2-2
- License compliance rectification

* Wed Oct 13 2021 peijiankang <peijiankang@kylinos.cn> - 5.15.2-1
- update to upstream version 5.15.2

* Mon Sep 14 2020 liuweibo <liuweibo10@huawei.com> - 5.11.1-6
- Fix Source0 

* Fri Feb 14 2020 Ling Yang <lingyang2@huawei.com> - 5.11.1-5
- Package init
